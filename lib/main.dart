import 'package:flutter/material.dart';
import 'package:time_passes/timer/home_page_timer.dart';

const double defaultFilling = 5.0;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Time handler',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: HomePageTimer(),
    );
  }
}
