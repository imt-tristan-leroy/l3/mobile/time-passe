import 'package:flutter/material.dart';
import 'package:time_passes/parameter/enums/parameter.dart';

typedef CallbackSetting = void Function(Parameter, int);

class ParameterButton extends StatelessWidget {
  final Color color;
  final String text;
  final int value;
  final Parameter parameter;
  final CallbackSetting action;

  const ParameterButton({
    Key? key,
    required this.color,
    required this.text,
    required this.value,
    required this.parameter,
    required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Text(
        text,
        style: const TextStyle(color: Colors.white),
      ),
      onPressed: () => action(parameter, value),
      color: color,
    );
  }
}
