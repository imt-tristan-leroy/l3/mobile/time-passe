import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:time_passes/parameter/parameter_button.dart';

import 'enums/parameter.dart';

class ParameterPage extends StatefulWidget {
  const ParameterPage({Key? key}) : super(key: key);

  @override
  State<ParameterPage> createState() => _ParameterPageState();
}

class _ParameterPageState extends State<ParameterPage> {
  TextEditingController txtWorkTime = TextEditingController();
  TextEditingController txtLittleBreakTime = TextEditingController();
  TextEditingController txtBigBreakTime = TextEditingController();

  @override
  initState() {
    readParameters();
    super.initState();
  }

  void readParameters() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? workTime = preferences.getInt(Parameter.workTime.name);
    if (workTime == null) {
      await preferences.setInt(
          Parameter.workTime.name, defaultParameters[Parameter.workTime] ?? 30);
    }
    int? littleBreakTime = preferences.getInt(Parameter.littleBreakTime.name);
    if (littleBreakTime == null) {
      await preferences.setInt(Parameter.littleBreakTime.name,
          defaultParameters[Parameter.littleBreakTime] ?? 5);
    }
    int? bigBreakTime = preferences.getInt(Parameter.bigBreakTime.name);
    if (bigBreakTime == null) {
      await preferences.setInt(Parameter.bigBreakTime.name,
          defaultParameters[Parameter.bigBreakTime] ?? 20);
    }
    setState(() {
      txtWorkTime.text = workTime.toString();
      txtLittleBreakTime.text = littleBreakTime.toString();
      txtBigBreakTime.text = bigBreakTime.toString();
    });
  }

  void updateParameters(Parameter key, int value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    switch (key) {
      case Parameter.workTime:
        int time = preferences.getInt(Parameter.workTime.name) ??
            defaultParameters[Parameter.workTime] ??
            30;
        time += value;
        if (time >= 1 && time <= 180) {
          print(time);
          preferences.setInt(Parameter.workTime.name, time);
        }
        setState(() {
          txtWorkTime.text = time.toString();
        });
        break;
      case Parameter.littleBreakTime:
        int time = preferences.getInt(Parameter.littleBreakTime.name) ??
            defaultParameters[Parameter.littleBreakTime] ??
            30;
        time += value;
        if (time >= 1 && time <= 180) {
          preferences.setInt(Parameter.littleBreakTime.name, time);
        }
        setState(() {
          txtLittleBreakTime.text = time.toString();
        });
        break;
      case Parameter.bigBreakTime:
        int time = preferences.getInt(Parameter.bigBreakTime.name) ??
            defaultParameters[Parameter.bigBreakTime] ??
            30;
        time += value;
        if (time >= 1 && time <= 180) {
          preferences.setInt(Parameter.bigBreakTime.name, time);
        }
        setState(() {
          txtBigBreakTime.text = time.toString();
        });
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle textStyle = TextStyle(
      fontSize: 20,
    );

    return Scaffold(
        appBar: AppBar(
          title: const Text('Parameters'),
        ),
        body: GridView.count(
          crossAxisCount: 3,
          children: [
            const Text(
              "Work Time",
              style: textStyle,
            ),
            const Text(""),
            const Text(""),
            ParameterButton(
                color: Colors.black26,
                text: "-",
                value: -1,
                parameter: Parameter.workTime,
                action: updateParameters),
            TextField(
              keyboardType: TextInputType.number,
              controller: txtWorkTime,
            ),
            ParameterButton(
                color: Colors.greenAccent,
                text: "+",
                value: 1,
                parameter: Parameter.workTime,
                action: updateParameters),
            const Text(
              "Little Break Time",
              style: textStyle,
            ),
            const Text(""),
            const Text(""),
            ParameterButton(
                color: Colors.black26,
                text: "-",
                value: -1,
                parameter: Parameter.littleBreakTime,
                action: updateParameters),
            TextField(
              keyboardType: TextInputType.number,
              controller: txtLittleBreakTime,
            ),
            ParameterButton(
                color: Colors.greenAccent,
                text: "+",
                value: 1,
                parameter: Parameter.littleBreakTime,
                action: updateParameters),
            const Text(
              "Big Break Time",
              style: textStyle,
            ),
            const Text(""),
            const Text(""),
            ParameterButton(
                color: Colors.black26,
                text: "-",
                value: -1,
                parameter: Parameter.bigBreakTime,
                action: updateParameters),
            TextField(
              keyboardType: TextInputType.number,
              controller: txtBigBreakTime,
            ),
            ParameterButton(
                color: Colors.greenAccent,
                text: "+",
                value: 1,
                parameter: Parameter.bigBreakTime,
                action: updateParameters),
          ],
        ));
  }
}
