enum Parameter {
  workTime,
  littleBreakTime,
  bigBreakTime,
}

final Map<Parameter, int> defaultParameters = {
  Parameter.workTime: 30,
  Parameter.littleBreakTime: 5,
  Parameter.bigBreakTime: 20
};
