import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:time_passes/generic/generic_button.dart';
import 'package:time_passes/parameter/parameter_page.dart';
import 'package:time_passes/timer/timer.dart';
import 'package:time_passes/timer/timer_model.dart';

import '../main.dart';

class HomePageTimer extends StatelessWidget {
  final Timer timer = Timer();
  final List<PopupMenuItem<String>> pages = [
    const PopupMenuItem(value: "parameters", child: Text("Parameters"))
  ];

  HomePageTimer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Timer'),
          actions: [
            PopupMenuButton(itemBuilder: (BuildContext context) {
              return pages.toList();
            }, onSelected: (s) {
              if (s == "parameters") {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ParameterPage()));
              }
            }),
          ],
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            final double withAvailable = constraints.maxWidth;
            return Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GenericButton(
                              color: Colors.greenAccent,
                              text: "Work",
                              size: defaultFilling,
                              action: () => timer.startWork())),
                    ),
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GenericButton(
                              color: Colors.grey,
                              text: "Little break",
                              size: defaultFilling,
                              action: () => timer.startBreak())),
                    ),
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GenericButton(
                              color: Colors.black26,
                              text: "Big break",
                              size: defaultFilling,
                              action: () => timer.startBreak(long: true))),
                    ),
                  ],
                ),
                Expanded(
                    child: StreamBuilder(
                  initialData: TimerModel('00:00', 1),
                  stream: timer.stream(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    TimerModel timer = snapshot.data;
                    return Container(
                      child: CircularPercentIndicator(
                        radius: withAvailable / 2,
                        lineWidth: 10.0,
                        percent:
                            (timer.percentage == null) ? 1 : timer.percentage,
                        center: Text(
                          (timer.time == null) ? '00:00' : timer.time,
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        progressColor: const Color(0xff009688),
                      ),
                    );
                  },
                )),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GenericButton(
                              color: Colors.black,
                              text: "Pause the timer",
                              size: defaultFilling,
                              action: () => timer.pauseTimer())),
                    ),
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GenericButton(
                              color: Colors.greenAccent,
                              text: "Restart the timer",
                              size: defaultFilling,
                              action: () => timer.restartTimer())),
                    ),
                  ],
                ),
              ],
            );
          },
        ));
  }
}
