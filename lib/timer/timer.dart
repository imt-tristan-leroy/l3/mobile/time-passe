import 'package:shared_preferences/shared_preferences.dart';
import 'package:time_passes/timer/timer_model.dart';

import '../parameter/enums/parameter.dart';

class Timer {
  double _radius = 1;
  bool _isActive = false;
  Duration _time = const Duration();
  Duration _totalTime = const Duration();

  int workTime = defaultParameters[Parameter.workTime] ?? 30;
  int littleBreakTime = defaultParameters[Parameter.workTime] ?? 5;
  int bigBreakTime = defaultParameters[Parameter.workTime] ?? 20;

  void readParameters() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    int? workTime = preferences.getInt(Parameter.workTime.name);
    if (workTime == null) {
      this.workTime = defaultParameters[Parameter.workTime] ?? 30;
      await preferences.setInt(Parameter.workTime.name, this.workTime);
    } else {
      this.workTime = workTime;
    }

    int? littleBreakTime = preferences.getInt(Parameter.littleBreakTime.name);
    if (littleBreakTime == null) {
      this.littleBreakTime = defaultParameters[Parameter.littleBreakTime] ?? 5;
      await preferences.setInt(
          Parameter.littleBreakTime.name, this.littleBreakTime);
    } else {
      this.littleBreakTime = littleBreakTime;
    }

    int? bigBreakTime = preferences.getInt(Parameter.bigBreakTime.name);
    if (bigBreakTime == null) {
      this.bigBreakTime = defaultParameters[Parameter.bigBreakTime] ?? 20;
      await preferences.setInt(Parameter.bigBreakTime.name, this.bigBreakTime);
    } else {
      this.bigBreakTime = bigBreakTime;
    }
  }

  void startWork() async {
    readParameters();
    _time = Duration(minutes: workTime);
    _totalTime = Duration(minutes: workTime);
    _isActive = true;
  }

  void startBreak({bool long = false}) async {
    readParameters();
    _time = Duration(minutes: long ? bigBreakTime : littleBreakTime);
    _totalTime = Duration(minutes: long ? bigBreakTime : littleBreakTime);
    _isActive = true;
  }

  void pauseTimer() {
    _isActive = false;
  }

  void restartTimer() {
    if (_time.inSeconds >= 0) {
      _isActive = true;
    }
  }

  String returnTime(Duration t) {
    String minutes = (t.inMinutes < 10)
        ? '0' + t.inMinutes.toString()
        : t.inMinutes.toString();
    int numSeconds = t.inSeconds - (t.inMinutes * 60);
    String seconds =
        (numSeconds < 10) ? '0' + numSeconds.toString() : numSeconds.toString();
    return minutes + ':' + seconds;
  }

  Stream<TimerModel> stream() async* {
    yield* Stream.periodic(const Duration(seconds: 1), (int a) {
      String time;
      if (_isActive) {
        _time = _time - const Duration(seconds: 1);
        _radius = _time.inSeconds / _totalTime.inSeconds;
        if (_time.inSeconds <= 0) {
          _isActive = false;
        }
      }
      time = returnTime(_time);
      return TimerModel(time, _radius);
    });
  }
}
