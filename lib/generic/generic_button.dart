import 'package:flutter/material.dart';

class GenericButton extends StatelessWidget {
  final Color color;
  final String text;
  final double size;
  final VoidCallback action;

  const GenericButton({
    Key? key,
    required this.color,
    required this.text,
    required this.size,
    required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      onPressed: action,
      color: color,
      minWidth: size,
    );
  }
}
